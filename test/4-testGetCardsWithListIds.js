const getCards = require('../4-getCardsWithListId');
const listId = '66541d23a87771400bdcf94e';

getCards(listId)
    .then((data) => {
        console.log(data);
    }).catch((error) => {
        console.log(`Error in getting cards by listID: ${error}`);
    })