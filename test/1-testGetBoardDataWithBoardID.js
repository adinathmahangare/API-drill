const getBoard = require('../1-getBoardDataWithBoardID');
const boardId = '66541b193a1f8f777e81af23';

getBoard(boardId)
    .then((data) => {
        console.log(data);
    }).catch((error) => {
        console.log(`Error in getting board by ID: ${error}`);
    });