const createNewBoard = require('./../6-newBoard3ListsCard');
const deleteList = require('./../7-deleteListsInBoard');

createNewBoard('deleteListsBoard')
.then((cards) => {
    let deletePromises = [];
        for(let card of cards){
            let deletePromise = deleteList(card.idList);
            deletePromises.push(deletePromise);
        }

        return Promise.all(deletePromises);
    })
    .then((data) => {
        console.log(data);
    })
    .catch((error) => {
        console.log(`Error in deleting lists: ${error}`);
    });

