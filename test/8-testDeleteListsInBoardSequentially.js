const createNewBoard = require('./../6-newBoard3ListsCard');
const deleteList = require('./../7-deleteListsInBoard');

createNewBoard('deleteListsBoard')
.then((createdCards) => {
    return createdCards.reduce((promise, card) => {
        return promise.then(() => {
            return deleteList(card.idList)
        })
    }, Promise.resolve());
    })
    .then((data) => {
        console.log(data);
    })
    .catch((error) => {
        console.log(`Error in deleting lists: ${error}`);
    });

