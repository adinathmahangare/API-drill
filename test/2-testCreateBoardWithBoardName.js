const getBoardData = require('../2-createBoardWithBoardName');
const boardName = 'APIdrill2';

getBoardData(boardName)
    .then((data) => {
        console.log(data);
    }).catch((error) => {
        console.log(`Error in creating board by Name: ${error}`);
    })