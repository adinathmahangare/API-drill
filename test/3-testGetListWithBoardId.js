const getList = require('../3-getListsWithBoardId');
const boardId = '66541b193a1f8f777e81af23';

getList(boardId)
    .then((data) => {
        console.log(data);
    }).catch((error) => {
        console.log(`Error in getting board by ID: ${error}`);
    })