const checkListID = '665482e0edaf8d5979309c6d';
const cardID = '66541d0f861ff8bebbbee9e2';
const { getCheckItems, updateCheckItem } = require('./../9-updateAllCheckItemsToCompleted');

getCheckItems(checkListID)
    .then((checkItems) => {
        let promises = [];
        for(let item of checkItems){
            let completePromise = updateCheckItem(cardID, item.id, true)
            promises.push(completePromise);
        }
        return Promise.all(promises);
    })
    .then((data) => {
        console.log(data);
    })
    .catch((error) => {
        console.log(`Error in updating checkItems state: ${error}`);
    })

