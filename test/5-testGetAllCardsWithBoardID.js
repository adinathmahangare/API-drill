const getAllCards = require('../5-getAllCardsWithBoardID');
let boardID = '66541b193a1f8f777e81af23';

getAllCards(boardID)
.then((data) => {
    console.log(data);
}).catch((error) => {
    console.log(`Error in getting board by ID: ${error}`);
});