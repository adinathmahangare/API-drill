const checkListID = '665482e0edaf8d5979309c6d';
const cardID = '66541d0f861ff8bebbbee9e2';
const { getCheckItems, updateCheckItem } = require('./../9-updateAllCheckItemsToCompleted');

getCheckItems(checkListID)
    .then((checkItems) => {
        return checkItems.reduce((promise, item) => {
            return promise.then(() => {
                return updateCheckItem(cardID, item.id, false)
            })
        }, Promise.resolve());
    })
    .then((data) => {
        console.log(data);
    })
    .catch((error) => {
        console.log(`Error in updating checkItems state: ${error}`);
    })

