//Create a function getAllCards which takes a boardId as argument and which uses getCards function to fetch cards of all the lists. Do note that the cards should be fetched simultaneously from all the lists.

const getCards = require('./4-getCardsWithListId');
const api_key = '3ab9825df63d6da5d1b6029513c6f7de';
const token_key = 'ATTA7703ae542b05a2e5ab6cc3bc6f90f32966c336ef8162bded6369b3ff496d506b25566DF9';

function getAllCards(boardId){

    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${api_key}&token=${token_key}`, {
            method: "GET"
        })
        .then(response => response.json())
        .then((data) => {

            let promises = []
            const listIDs = data.map((list) => list.id);
            for (let listID of listIDs){
                let promise = getCards(listID);
                promises.push(promise);
            }

            Promise.all(promises)
                .then((data) => {
                    if(data){
                        resolve(data);
                    }
                })

            
        }).catch((error) => {
            reject(error);
        });
    });
}

module.exports = getAllCards;
