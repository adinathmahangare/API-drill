//Create a new board, create 3 lists simultaneously, and a card in each list simultaneously

const api_key = '3ab9825df63d6da5d1b6029513c6f7de';
const token_key = 'ATTA7703ae542b05a2e5ab6cc3bc6f90f32966c336ef8162bded6369b3ff496d506b25566DF9';

function createList(boardID, listName){
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/lists?name=${listName}&idBoard=${boardID}&key=${api_key}&token=${token_key}`, {
            method: 'POST'
        })
        .then(response => response.json())
        .then(data => resolve(data))
        .catch(err => reject(err));
    });
}

function createCard(listID){
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/cards?idList=${listID}&key=${api_key}&token=${token_key}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
        .then(response => response.json())
        .then(data => resolve(data))
        .catch(err => reject(err));
    })
}

function createNewBoard(boardName) {
    
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/boards/?name=${boardName}&key=${api_key}&token=${token_key}`, {
            method: "POST",
        })
        .then(response => response.json())
        .then((board) => {
            const listPromises = [];
    
            const listNames = ["List1", "List2", "List3"];

            for (let i = 0; i < 3; i++) {
                listPromises.push(
                    createList(board.id, listNames[i]).then((list) => createCard(list.id))
                );
            }
            return Promise.all(listPromises);
        })
        .then((createdLists) => {
            resolve(createdLists);
        })
        .catch((error) => {
            // Handle errors from createBoard or any other errors in the chain
            console.error('Error creating board or lists/cards:', error);
            reject(error);
        });

    });
} 

module.exports = createNewBoard;
