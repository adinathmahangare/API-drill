//Update all checkitems in a checklist to completed status sequentially
const api_key = '3ab9825df63d6da5d1b6029513c6f7de';
const token_key = 'ATTA7703ae542b05a2e5ab6cc3bc6f90f32966c336ef8162bded6369b3ff496d506b25566DF9';

function getCheckItems(checklistID){
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/checklists/${checklistID}/checkItems?key=${api_key}&token=${token_key}`)
            .then(response => response.json())
            .then((checkItems) => {
                resolve(checkItems);
            }).catch((error) => {
                reject(error);
            })
    })
}

function updateCheckItem(cardID, checkItemID, state){
    return new Promise((resolve, reject) => {
        let query = "state=" + (state ? "complete" : "incomplete");
        if(state){
            fetch(`https://api.trello.com/1/cards/${cardID}/checkItem/${checkItemID}?${query}&key=${api_key}&token=${token_key}`, {
                method: "PUT"
            })
            .then(response => response.json())
            .then((checkItem) => {
                resolve(checkItem);
            }).catch((error) => {
                reject(error);
            })
        } else {
            setTimeout(() => {
                fetch(`https://api.trello.com/1/cards/${cardID}/checkItem/${checkItemID}?${query}&key=${api_key}&token=${token_key}`, {
                    method: "PUT"
                })
                .then(response => response.json())
                .then((checkItem) => {
                    resolve(checkItem);
                }).catch((error) => {
                    reject(error);
                })
            }, 1000)
        }
    })
}

module.exports = {
    updateCheckItem, getCheckItems
}

