const api_key = '3ab9825df63d6da5d1b6029513c6f7de';
const token_key = 'ATTA7703ae542b05a2e5ab6cc3bc6f90f32966c336ef8162bded6369b3ff496d506b25566DF9';

function getBoard(boardID) {
    
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/boards/${boardID}?key=${api_key}&token=${token_key}`, {
            method: "GET"
        })
        .then(response => response.json())
        .then((data) => {
            resolve(data);
        }).catch((error) => {
            reject(error);
        });
    });
} 

// function getAPIData (){
//     fetch(`https://api.trello.com/1/members/me/boards?key=${api_key}&token=${token_key}`)
//     .then((response) => {
//         response.json()
//         .then((data) => console.log(data));
//     })
// }

module.exports = getBoard;